#### How to set up a modern JavaScript project [Tutorial]
	
    - you decide to go with the frontend application, by not linking JavaScript files in HTML files and automate this process for you. popular ways to start with a JavaScript project nowadays. 
	
	- create directory to allocate project's configuration but most importantly all of its source code
	
		mkdir my-project
		cd my-project
	
	-  initialize the project as npm project
	
		- install libraries (node packages)
			- show up in the package.json as dependency
			- node_modules/ folder
				- installed libraries will be kept
				
		- npm scripts to start, test, or deploy your project in a later stage	
		
		- npm init -y
			- take all the defaults otherwise specify the information manually
			
			npm config list
			npm set init.author.name "<Your Name>"
			npm set init.author.email "you@example.com"
			npm set init.author.url "example.com"
			npm set init.license "MIT"
			
		- create an entry point to your project
		
			mkdir src
			cd src
			touch index.js
			
		- write your first JavaScript src/index.js
		
			console.log('Hello Project.');
			
			
		- run this file with Node.js
			
			node src/index.js
			
		-  project's scripts (start, test, deploy) on just npm start add into package.json file
		
			{
			  ...
			  "scripts": {
				"start": "node src/index.js",
				"test": "echo \"Error: no test specified\" && exit 1"
			  },
			  ...
			}
			
			- Every time you change package.json file's npm scripts  need to type npm start o
		
			