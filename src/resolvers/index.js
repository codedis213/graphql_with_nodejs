import uuidv4 from 'uuid/v4';
import userResolvers from './user';
import messageResolvers from './message';
export default [userResolvers, messageResolvers];